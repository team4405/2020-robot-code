#define SENSOR1_OUTPUT_PIN 8
#define SENSOR2_OUTPUT_PIN 12
// Pin 13: Arduino has an LED connected on pin 13
// Pin 11: Teensy 2.0 has the LED on pin 11
// Pin 6: Teensy++ 2.0 has the LED on pin 6
// Pin 13: Teensy 3.0 has the LED on pin 13
#define SENSORPIN 4
#define SENSORPIN2 7
#define delayTimeMS 1000
// # of power cells in storage

int powerCellIndexer = 0; 
// variables will change:
int sensorState = 0, lastState=0, sensorState2 = 0, lastState2 = 0; // variable for reading the pushbutton status
int sensor1_st = 0; // Start time for sensor1
int sensor2_st = 0; // Start time for sensor2

void setup() {
// initialize the sensor pin as an input:
pinMode(SENSORPIN, INPUT);
pinMode(SENSORPIN2, INPUT);
pinMode(SENSOR1_OUTPUT_PIN, OUTPUT);
pinMode(SENSOR2_OUTPUT_PIN, OUTPUT);
digitalWrite(SENSORPIN, HIGH); // turn on the pullup
digitalWrite(SENSORPIN2, HIGH);
Serial.begin(9600);
}
void loop(){
  // read the state of the pushbutton value:
  sensorState = digitalRead(SENSORPIN);
  sensorState2 = digitalRead(SENSORPIN2);

  if (sensorState == true)
  {
    digitalWrite(SENSOR1_OUTPUT_PIN, HIGH);
  }
  else
  {
    digitalWrite(SENSOR1_OUTPUT_PIN, LOW);
  }
  
  if (sensorState2 == true)
  {
    digitalWrite(SENSOR2_OUTPUT_PIN, HIGH);
  }
  else
  {
    digitalWrite(SENSOR2_OUTPUT_PIN, LOW);
  }
  
  Serial.println(sensorState);
  Serial.println(sensorState2);
  // check if the sensor beam is broken
  // if it is, the sensorState is LOW:
 /* if (powerCellIndexer >= 1) {
  // turn LED on:
  digitalWrite(LEDPIN, HIGH);
  }
  else {
  // turn LED off:
    digitalWrite(LEDPIN, LOW);
  }
  if (sensorState && !lastState) {
    Serial.println("Unbroken");
  }
  if (powerCellIndexer < 5) {
    if (!sensorState && lastState) {
      Serial.println("Broken");
      powerCellIndexer ++;
      Serial.println(powerCellIndexer);
      delay(delayTimeMS);
    }
  }
  if (sensorState2 && !lastState2) {
    Serial.println("Sensor 2 Unbroken");
  }
  if (powerCellIndexer != 0) {
    if (!sensorState2 && lastState2) {
      Serial.println("sensor 2 Broken");
      powerCellIndexer --;
      Serial.println(powerCellIndexer);
      delay(delayTimeMS);
    } 

   
 }
  lastState = sensorState;
  lastState2 = sensorState2; */
} 
