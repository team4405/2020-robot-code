package frc.robot;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.Timer;

public class LEDS{
    
    /***********************LEDS********************************/
	public AddressableLED m_led;
	public AddressableLEDBuffer m_ledBuffer;
	public int port;
	public int green = 255;
	public int blue = 255;
	public int red = 255;
	public double LED_loop_delay = 0.00001;

    public LEDS(int user_port, int num_LEDs)
    {
		port = user_port;
         // PWM port 9
		// Must be a PWM header, not MXP or DIO
		m_led = new AddressableLED(port);

		// Reuse buffer
		// Default to a length of 60, start empty output
		// Length is expensive to set, so only set it once, then just update data
		m_ledBuffer = new AddressableLEDBuffer(num_LEDs);
		m_led.setLength(m_ledBuffer.getLength());

		// Set the data
		m_led.setData(m_ledBuffer);
		m_led.start();
    }

    public void scrollLEDS()
    {
        for (var current_LED = 0; current_LED < m_ledBuffer.getLength(); current_LED++) {
			// Sets the specified LED to the RGB values for red
			if (current_LED != 0){
				m_ledBuffer.setRGB((current_LED-1)% m_ledBuffer.getLength(), 0, 0, 0);
			}
			m_ledBuffer.setRGB(current_LED% m_ledBuffer.getLength(), red, green, blue);
			m_ledBuffer.setRGB((current_LED+1)% m_ledBuffer.getLength(), red, green, blue);
			m_ledBuffer.setRGB((current_LED+2)% m_ledBuffer.getLength(), red, green, blue);
			m_ledBuffer.setRGB((current_LED+3)% m_ledBuffer.getLength(), red, green, blue);
		 
			m_led.setData(m_ledBuffer);
			Timer.delay(LED_loop_delay);
		}
	}

	public void lightLEDS(int startLED, int endLED)
    {
        for (var current_LED = 0; current_LED < m_ledBuffer.getLength(); current_LED++) {
			// Sets the specified LED to the RGB values for red
			if (current_LED < startLED || current_LED > endLED){
				m_ledBuffer.setRGB(current_LED, 0, 0, 0);
			}
			else
			{
				m_ledBuffer.setRGB(current_LED, red, green, blue);
			}
			m_led.setData(m_ledBuffer);
			Timer.delay(LED_loop_delay);
		}
	}
	public int get_port()
	{
		return port;
	}

	public void set_green(int num)
	{
		green = num;
	}
	public void set_blue(int num)
	{
		blue = num;
	}
	public void set_red(int num)
	{
		red = num;
	}
	public void set_colors(int red_num, int green_num, int blue_num)
	{
		red = red_num; 
		green = green_num;
		blue = blue_num;
	}
	public void set_timer(double timer_bot)
	{
		LED_loop_delay = timer_bot;
	}
}
