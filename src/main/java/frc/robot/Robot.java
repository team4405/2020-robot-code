/******************************************************************************************************
*******************************************************************************************************
										LIBRARIES
******************************************************************************************************
*******************************************************************************************************/
package frc.robot;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//import sun.jvm.hotspot.debugger.cdbg.basic.LazyBlockSym;
//import edu.wpi.cscore.VideoMode.PixelFormat;
//import edu.wpi.first.cameraserver.CameraServer;
import com.revrobotics.*;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import com.kauailabs.navx.frc.AHRS;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.DigitalInput;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.TimedRobot;
//import edu.wpi.first.wpilibj.DigitalOutput;
import java.lang.Math;
import frc.robot.LIDAR;
import edu.wpi.first.wpilibj.AnalogInput;
/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends TimedRobot implements PIDOutput {
/******************************************************************************************************
*******************************************************************************************************
										GLOBAL VARIABLES
******************************************************************************************************
*******************************************************************************************************/

	/***********************LEDS********************************/
	LEDS leds = new LEDS(9, 5);
	//*********************PNEUMATICS**********************
	
	//*********************SENSORS**********************
	//Control of break beam sensors for managing power cells
	//Ultrasonics
	//Ultrasonic ultra_sonic = new Ultrasonic(1, 2);
	final AnalogInput ultrasonic = new AnalogInput(1);
	double rawUltraSonicValue = ultrasonic.getValue(); 
	double currentDistance = rawUltraSonicValue * 0.125;

	//the 0 represents the Analog port number we’ve connected our sensor to.
	DigitalInput powercellsIntakePIN = new DigitalInput(9);
	DigitalInput powercellsOutputPIN = new DigitalInput(8);
	boolean ignore_IntakeOutputSensors = false;
	long hopperToggleTime = 0;
	int powerCellsInRobot = 0;
	double intake_timeoutMS = 500;
	double output_timeoutMS = 500;
	boolean powercellsIntakePIN_already_true = false;
	boolean powercellsOutputPIN_already_true = false;
	double last_intake_time_powerCellMS = 0;
	double last_output_time_powerCellMS = 0;
	//Encoders
	double left_wheel_distance_traveled;
	double right_wheel_distance_traveled;
	double encoder_conversion_factor;
	//LIMELIGHT CODE
	NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
	NetworkTableEntry tx = table.getEntry("tx");
	NetworkTableEntry ty = table.getEntry("ty");
	NetworkTableEntry ta = table.getEntry("ta");
	NetworkTableEntry tv = table.getEntry("tv");

	//LIMELIGHT values to read periodically
	double x;
	double y;
	double area;
	double v;
	double distance_to_target;
	double x_offset;

	//LIDAR values
	double distanceFromTargetLIDAR;
	// LIDAR distance variables
	DigitalInput LIDARsource = new DigitalInput(4);
	LIDAR LIDARinput = new LIDAR(LIDARsource);

	//NAV-X
	AHRS NavX;
	double currentRotationRate;
	PIDController turnController;
	double last_angle;
	//PID
	/* The following PID Controller coefficients will need to be tuned */
	/* to match the dynamics of your drive system.  Note that the      */
	/* SmartDashboard in Test mode has support for helping you tune    */
	/* controllers by displaying a form where you can enter new P, I,  */
	/* and D constants and test the mechanism.                         */
	double kP = 0.03;
	double kI = 0.0;
	double kD = 0.0;

	boolean humanControl = true;
	static final double kF = 0.00;
	
	/* This tuning parameter indicates how close to "on target" the    */
	/* PID Controller will attempt to get.                             */

	static final double kToleranceDegrees = 1.0f;

	//*********************DRIVE**********************
	//Talon drive motors for 6 motor system
	WPI_TalonFX RB_2020 = new WPI_TalonFX(3);
	WPI_TalonFX LF_2020 = new WPI_TalonFX(1);
	WPI_TalonFX RF_2020 = new WPI_TalonFX(4);
	WPI_TalonFX LB_2020 = new WPI_TalonFX(2);
	//SpeedControllerGroup tester = new SpeedControllerGroup(RB_2020,RF_2020);

	MecanumDrive DriveTrain = new MecanumDrive(RF_2020, LF_2020, RB_2020, LB_2020 );
	boolean lastOrientation = false;
	//Joystick objects/variables
	Joystick left_joy_stick = new Joystick(2);
	double timeLastFlipped = 0;
	double hold_angle = 500;

	//Xbox Controller objects/variables
	XboxController xboxController_01 = new XboxController(0);
	XboxController xboxController_02 = new XboxController(1);
	double robotZ;
	double xBoxRightY;
	double robotX;
	double robotY;
	double RStick;
	double RAngle;
	double deadzone = 0.05;
	String driveControllerToUse = "XBOX";
	//*********************PNEUMATICS**********************
	Compressor intakearm = new Compressor();
	DoubleSolenoid intakeArmSolenoid = new DoubleSolenoid(0, 1);

	//*********************AUTON**********************
	//angle to be used for measuring gyro angle
	double angle = 0;
	//Drive power to use for auton
	double auton_drive_power = 0.50;
	//to keep track of where we are
	int auton_step = 0;
	String autonStart = "CENTER";

	boolean drive_Forward = true;
	boolean drive_Sideways = false;
	double driveEncoderCorrectionFactor = 0.75;
	double correctionSideways = -0.25;
	double driveSpeed_forward = .55;
	double driveSpeed_Sideways = .35;
	long startAutonStepTime = -1;
	double autonC_initBackup = 95.125;
	double autonC_center2Wall = 120.66;
	double autonC_wall2trench = 125;
	double autonL_initBackup = 95.125;
	double autonR_initBackup = 75;
	double autonR_wall2trench = 120.66;
	String auton_Powercellretrive = "6";
	String auton_drivedirection = "back";
	double rightautonbackupangle = 0;
	double rightautonwallangle = 10;
	//*********************OTHER MOTORS**********************
	CANSparkMax elevator_motor = new CANSparkMax(6,MotorType.kBrushless);
	CANSparkMax shooter_motor = new CANSparkMax(7,MotorType.kBrushless);
	CANPIDController shooter_motorPID;// = new CANPIDController(shooter_motor);
	CANEncoder shooter_encoder;
	CANSparkMax hopper_motor = new CANSparkMax(8,MotorType.kBrushless);
	double hopperMotorSpeed = -1.00;
	Spark elevatorslide_motor = new Spark(0);
	CANSparkMax intakemotor = new CANSparkMax(10,MotorType.kBrushless);
	boolean shootingMotorRamped = false;
	double shootingMotorSpeed = -1;
	double shootingMotorSpeedHighPower = -.73;
	long shootingRampTime = 1;
	boolean shootingMotorOn = false;
	long rampStartTime = -1;
	long timeLastShot = 0;
	boolean isCurrentlyShooting = false;
	boolean hopperOveride = false;
	double lastShooterButtonPressMS = 0;
	double intakeMotorSpeed = -0.3;
	boolean hopperPCdelay = false;
	long hopperPCtime = 0;
	double extraTimePC = 150;
	long armClosedTime = 0;
	boolean armClosed = true;
	double desiredShooterMotorSpeed = -3840;
	double shooting_motor_velocity_ABS = 0;

	//*********************MISC**********************
	//Cuts down on the prints
	int print_counter = 0;
	int print_frequency = 100;
	String gameData;
	long current_time = System.currentTimeMillis();

/******************************************************************************************************
*******************************************************************************************************
										ROBOT INIT FUNCTIONS
******************************************************************************************************
*******************************************************************************************************/
	public Robot() {
		try {
	        NavX = new AHRS(SPI.Port.kMXP); 
	    } catch (RuntimeException ex ) {
			DriverStation.reportError("Error instantiating navX-MXP:  " + ex.getMessage(), true);
		}
		
		turnController = new PIDController(kP, kI, kD, kF, NavX, this);
		turnController.setInputRange(-180.0f,  180.0f);
      turnController.setOutputRange(-0.5, 0.5);
      turnController.setAbsoluteTolerance(kToleranceDegrees);
	  turnController.setContinuous(true);
	  turnController.disable();
	}
	
	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() {
		intakearm.close();
		SmartDashboard.putNumber("Ultra Sonic Distance", currentDistance );
		SmartDashboard.putString("Auton start position (left, right, center)", autonStart);
		SmartDashboard.putString("Color wheel deisired color: ", "UNKNOWN");
		SmartDashboard.putNumber("starting number of powercells auton", 3);
		SmartDashboard.putNumber("PID - P: ", kP);
		SmartDashboard.putNumber("PID - I: ", kI);
		SmartDashboard.putNumber("PID - D: ", kD);
		SmartDashboard.putNumber("Auton SLIDE Correction: ", correctionSideways);
		SmartDashboard.putString("Controller to use (enter either 'XBOX' or 'Joystick'): ", driveControllerToUse);		
		SmartDashboard.putNumber("Hopper (indexer) motor power: ", hopperMotorSpeed);
		SmartDashboard.putNumber("Shooter motor power: ", shootingMotorSpeed);
		SmartDashboard.putNumber("High Power Shooter motor power: ", shootingMotorSpeedHighPower);
		SmartDashboard.putNumber("Intake motor power: ", intakeMotorSpeed);
		SmartDashboard.putNumber("Auton drive speed: ", driveSpeed_forward);
		SmartDashboard.putNumber("Extra time between Power Cells: ", extraTimePC);
		SmartDashboard.putNumber("Desired SHooter motor speed: ", desiredShooterMotorSpeed);
		SmartDashboard.putNumber("CENTER - init backup: ", autonC_initBackup);
		SmartDashboard.putNumber("CENTER - center to wall: ", autonC_center2Wall);
		SmartDashboard.putNumber("CENTER - wall to-through trenchrun: ", autonC_wall2trench);
		SmartDashboard.putNumber("LEFT - init backup: ", autonL_initBackup);
		SmartDashboard.putNumber("RIGHT - init backup: ", autonR_initBackup);
		SmartDashboard.putNumber("RIGHT - wall to-through trenchrun: ", autonR_wall2trench);
		SmartDashboard.putString("Auton - only get three powercells:", auton_Powercellretrive);
		SmartDashboard.putString("Auton - drive forward: ", auton_drivedirection);
		SmartDashboard.putNumber("RIGHT - wall angle", rightautonwallangle);
		SmartDashboard.putNumber("RIGHT - back up angle", rightautonbackupangle);
		SmartDashboard.putNumber("PID - shooter motor velocity", desiredShooterMotorSpeed);
		SmartDashboard.putNumber("tx", tx.getDouble(0));
		SmartDashboard.putNumber("ty", ty.getDouble(0));
		SmartDashboard.putNumber("tv", tv.getDouble(0));
		SmartDashboard.putNumber("ta", ta.getDouble(0));
		SmartDashboard.putNumber("Distance to target: ", distance_to_target);
		RB_2020.setNeutralMode(NeutralMode.Coast);
		LB_2020.setNeutralMode(NeutralMode.Coast);
		RF_2020.setNeutralMode(NeutralMode.Coast);
		LF_2020.setNeutralMode(NeutralMode.Coast);
		NavX.reset();
		intakemotor.setIdleMode(IdleMode.kCoast);
		shooter_motor.setIdleMode(IdleMode.kCoast);
		shooter_encoder = shooter_motor.getEncoder();
		shooter_motorPID = shooter_motor.getPIDController();
		shooter_motorPID.setP(0.03);
		shooter_motorPID.setI(0);
		shooter_motorPID.setD(0.5);
		shooter_motorPID.setIZone(0);
		shooter_motorPID.setFF(0);
		shooter_motorPID.setOutputRange(-0.8,-0.5);
	

		shooter_motorPID.setSmartMotionMaxVelocity(5676, 2);
		shooter_motorPID.setSmartMotionMinOutputVelocity(0, 2);
		shooter_motorPID.setSmartMotionMaxAccel(1500, 2);
		shooter_motorPID.setSmartMotionAllowedClosedLoopError(5, 2);

		current_time = System.currentTimeMillis();

		elevator_motor.setIdleMode(IdleMode.kBrake);
	}
/******************************************************************************************************
*******************************************************************************************************
										TELEOP FUNCTIONS
******************************************************************************************************
*******************************************************************************************************/	
	/**
	 * This function is called once each time the robot enters tele-operated
	 * mode
	 */
	@Override
	public void teleopInit() {
		humanControl = true;
		//turnController.enable();
		//turnController.setSetpoint(NavX.getAngle());
		
		last_angle = 0;
		RB_2020.configFactoryDefault();
		LB_2020.configFactoryDefault();
		RF_2020.configFactoryDefault();
		LF_2020.configFactoryDefault();
		RB_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		LB_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		RF_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		LF_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		RB_2020.setNeutralMode(NeutralMode.Coast);
		LB_2020.setNeutralMode(NeutralMode.Coast);
		RF_2020.setNeutralMode(NeutralMode.Coast);
		LF_2020.setNeutralMode(NeutralMode.Coast);
		/*
		* choose whatever you want so "positive" values moves mechanism forward,
		* upwards, outward, etc.
		*
		* Note that you can set this to 	whatever you want, but this will not fix motor
		* output direction vs sensor direction.
		*/
		RB_2020.setInverted(false);
		LB_2020.setInverted(false);
		RF_2020.setInverted(false);
		LF_2020.setInverted(false);

		/*
		* flip value so that motor output and sensor velocity are the same polarity. Do
		* this before closed-looping
		*/
		RB_2020.setSensorPhase(false); // <<<<<< Adjust this
		LB_2020.setSensorPhase(false);
		RF_2020.setSensorPhase(false);
		LF_2020.setSensorPhase(false);

		//Set deadzone
		RB_2020.configNeutralDeadband(10);
		RF_2020.configNeutralDeadband(10);
		LB_2020.configNeutralDeadband(10);
		LF_2020.configNeutralDeadband(10);
		
		table = NetworkTableInstance.getDefault().getTable("limelight");
		tx = table.getEntry("tx");
		ty = table.getEntry("ty");
		ta = table.getEntry("ta");
		tv = table.getEntry("tv");
		elevator_motor.setIdleMode(IdleMode.kBrake);

		driveControllerToUse = SmartDashboard.getString("Controller to use (enter either 'XBOX' or 'Joystick'): ", driveControllerToUse);
		current_time = System.currentTimeMillis();
		
	}

	/**
	 * This function is called periodically during operator control
	 */
	@Override
	public void teleopPeriodic() {
		/**********************************ULTRASONIC***************************** */
		 rawUltraSonicValue = ultrasonic.getValue(); 
		 currentDistance = rawUltraSonicValue * 0.125;
		 SmartDashboard.putNumber("Ultra Sonic Distance", currentDistance );
		/********************************LIMELIGHT******************************** */
		table = NetworkTableInstance.getDefault().getTable("limelight");
		tx = table.getEntry("tx");
		ty = table.getEntry("ty");
		ta = table.getEntry("ta");
		tv = table.getEntry("tv");
		//read values periodically
		x = tx.getDouble(0.0) + x_offset;
		y = ty.getDouble(0.0);
		area = ta.getDouble(0.0);
		v = tv.getDouble(-1);

		distance_to_target = ((29 - 5.5) / java.lang.Math.tan(5 + ty.getDouble(0)));
		/******************************Toggle HOpper motor */
		if((xboxController_01.getStartButton() == true ||
		    xboxController_01.getBackButton()  == true ||
		    xboxController_02.getStartButton() == true ||
		    xboxController_02.getBackButton()  == true ) &&
		   current_time > hopperToggleTime + 200)
		{
			hopperToggleTime = current_time;
			//Toggle the state of the sensor
			ignore_IntakeOutputSensors = !ignore_IntakeOutputSensors;
		}

		current_time = System.currentTimeMillis();
		/**********************************************NavX CONTROLS******************** */
		angle = NavX.getAngle(); 
		/**********************************************LiDAR CONTORLS******************* */
		try
		{
			distanceFromTargetLIDAR();
		}
		catch (Exception ex)
		{
			System.out.println("LIDAR not connected");
			return;
		}
		/**********************************************ELEVATOR MOTOR CONTORLS******************* */
		if(Math.abs(xboxController_02.getY(Hand.kLeft)) > deadzone * 3)
		{
			elevator_motor.set(xboxController_02.getY(Hand.kLeft));
		}
		else
		{
			elevator_motor.set(0);
		}
		if(Math.abs(xboxController_02.getX(Hand.kRight)) > deadzone)
		{
			elevatorslide_motor.set(-xboxController_02.getX(Hand.kRight));
		}
		else
		{
			elevatorslide_motor.set(0);
		}

		/**********************************************INTAKE ARM SOLENOID********************* */
		if ((/*xboxController_02.getBumper(Hand.kLeft) ||*/ xboxController_01.getBumper(Hand.kLeft))
			&& current_time > armClosedTime + 300)
		{
			//toggle arm closed
			armClosed = !armClosed;
			armClosedTime = current_time;
			if(armClosed)
			{
				intakeArmSolenoid.set(Value.kForward);
			}
			else
			{
				intakeArmSolenoid.set(Value.kReverse);
				intakemotor.set(intakeMotorSpeed);
			}
		}
		if(armClosed == true)
		{
			if(current_time > armClosedTime + 1000)
			{
				intakemotor.set(0);
			}
			else
			{
				intakemotor.set(-intakeMotorSpeed);
			}
		}
		/**********************************************INTAKE/HOPPER MOTOR CONTROLS********************* */
		if (xboxController_02.getTriggerAxis(Hand.kRight)>deadzone || xboxController_01.getTriggerAxis(Hand.kRight)>deadzone)
		{
			hopper_motor.set(hopperMotorSpeed);
			hopperOveride = true;
		}
		else if(xboxController_02.getTriggerAxis(Hand.kLeft)>deadzone /*|| xboxController_01.getTriggerAxis(Hand.kLeft)>deadzone*/)
		{
			hopper_motor.set(-hopperMotorSpeed);
			hopperOveride = true;
		}
		//If we are to ignore the sensors, just kill the motors
		else if(ignore_IntakeOutputSensors)
		{
			hopper_motor.set(0);
		}
		else
		{
			if (powercellsIntakePIN.get() == false)
			{
				hopper_motor.set(hopperMotorSpeed);
				hopperPCdelay = true;
			}
			else
			{
				if (hopperPCdelay == true)
				{
					hopperPCtime = current_time;
					hopperPCdelay = false;
				}

				if (current_time > hopperPCtime + extraTimePC)
				{
					hopper_motor.set(0);
				}
			}			
			hopperOveride = false;
		}
	
		// if (powerCellsInRobot < 5)
		// {
		// 	intakemotor.set(0.75);
		// }
		/**********************************************SHOOTING MOTOR CONTROLS********************* */
		if(xboxController_01.getAButton() == true || xboxController_02.getAButton() == true)
		{
			shootingMotorOn = false;
			shooter_motor.set(0);
		}
		if (xboxController_01.getYButton() == true || xboxController_02.getYButton() == true)
		{
			shooter_motor.set(shootingMotorSpeedHighPower);
			shootingMotorOn = true;
		}
		if(xboxController_02.getXButton() == true || xboxController_01.getBumper(Hand.kRight) == true ) 
			{
			lastShooterButtonPressMS = current_time;
			shootingMotorOn = true;
			shooting_motor_velocity_ABS = Math.abs(shooter_encoder.getVelocity());
			if(shootingMotorOn)
			{
				//shooter_motorPID.setReference(desiredShooterMotorSpeed, ControlType.kVelocity);
				//shooter_motorPID.setReference(, ctrl);
				shooter_motor.set(shootingMotorSpeed);
				// if(Math.abs(desiredShooterMotorSpeed) > shooting_motor_velocity_ABS)
				// {
				// 	shooter_motor.set(shootingMotorSpeed * (1+((Math.abs(desiredShooterMotorSpeed) - shooting_motor_velocity_ABS) / Math.abs(desiredShooterMotorSpeed))));
				// }
				// else
				// {
				// 	shooter_motor.set(shootingMotorSpeed * (1-((shooting_motor_velocity_ABS - Math.abs(desiredShooterMotorSpeed)) / Math.abs(desiredShooterMotorSpeed))));
				// }
				
			}
			else
			{
				shooter_motor.set(0);
			}
		}
		System.out.println("Applied output: " + shooter_motor.getAppliedOutput());
		System.out.println("Process variable: " + shooter_encoder.getPosition());
		System.out.println("Velocity: " + shooter_encoder.getVelocity());

		/**********************************************DRIVING MOTOR CONTROLS********************* */
		if (driveControllerToUse.equalsIgnoreCase("JOYSTICK"))
		{
			robotZ = (left_joy_stick.getZ());
			robotX = (left_joy_stick.getX());
			robotY = -(left_joy_stick.getY());
			xBoxRightY = -(xboxController_01.getY(Hand.kRight));
		}
		else
		{
			robotZ = (xboxController_01.getX(Hand.kRight));
			xBoxRightY = -(xboxController_01.getY(Hand.kRight));
			robotX = (xboxController_01.getX(Hand.kLeft));
			robotY = -(xboxController_01.getY(Hand.kLeft));
		}
		RStick = Math.atan (Math.abs(robotZ + .000001/xBoxRightY + .000001));
		RAngle = (RStick) * (180/Math.PI);

		print_counter++;
		
		if(Math.abs(robotZ) > deadzone ||
			Math.abs(robotX) > deadzone ||
		    Math.abs(robotY) > deadzone )
		{
			turnController.disable();
			humanControl = true;
		}

		if (!humanControl)
		{
			DriveTrain.driveCartesian(0, 0, -currentRotationRate);	
		}
		else
		{
			if(Math.abs(robotZ) > deadzone ||
			Math.abs(robotX) > deadzone ||
		    Math.abs(robotY) > deadzone )
			{	
				if(xboxController_01.getStickButton(Hand.kLeft) && current_time > timeLastFlipped + 250)
				{
					lastOrientation = !lastOrientation;
					timeLastFlipped = current_time;
				}
				//If the driver selects this button, then we want to hold the angle, but they must hold the button
				if(xboxController_01.getYButton() == true && hold_angle > 400)
				{
					hold_angle = angle;
				}
				//If the do not select the button, set the hold angle to an impossible value
				else
				{
					hold_angle = 500;
				}
				if(lastOrientation == true)
				{
					//If the hold angle is less than 400, that means that the driver is holding the Y button
					//and therefore wants the robot to face forward
					if(hold_angle < 400 && false)
					{
						DriveTrain.driveCartesian(robotY, -robotX, hold_angle);
					}
					else
					{
						DriveTrain.driveCartesian(robotY, -robotX, -robotZ);
					}
				}
				else
				{
					//If the hold angle is less than 400, that means that the driver is holding the Y button
					//and therefore wants the robot to face forward
					if(hold_angle < 400 && false)
					{
						DriveTrain.driveCartesian(-robotY, robotX, hold_angle);
					}
					else
					{
						DriveTrain.driveCartesian(-robotY, robotX, -robotZ);
					}
				}
			}
		}
		/********************************************INTAKE, HOOPER (INDEXER) AND SHOOTING MOTOR MANAGEMENT ************************ */
		//trackPowerCells();
		//manageShootingMotor();


		/********************************************PRINTING************************ */
		if ( print_counter % print_frequency == 0) 
		{
		
		}
		/*******************************************SMART DASHBOARD****************** */		
		gameData = DriverStation.getInstance().getGameSpecificMessage();
		if(gameData.length() > 0)
		{
			switch (gameData.charAt(0))
			{
				case 'B' :
					SmartDashboard.putString("Color wheel deisired color: ", "BLUE");
					break;
				case 'G' :
					SmartDashboard.putString("Color wheel deisired color: ", "GREEN");
					break;
				case 'R' :
					SmartDashboard.putString("Color wheel deisired color: ", "RED");
					break;
				case 'Y' :
					SmartDashboard.putString("Color wheel deisired color: ", "YELLOW");
					break;
				default :
					SmartDashboard.putString("Color wheel deisired color: ", "UNKNOWN");
					break;
			}
		} 
		else 
		{
		//Code for no data received yet
		}
		kP = SmartDashboard.getNumber("PID - P: ", kP);
		kI = SmartDashboard.getNumber("PID - I: ", kI);
		kD = SmartDashboard.getNumber("PID - D: ", kD);
		correctionSideways =  SmartDashboard.getNumber("Auton SLIDE Correction: ", correctionSideways);		
		driveControllerToUse = SmartDashboard.getString("Controller to use (enter either 'XBOX' or 'Joystick'): ", driveControllerToUse);
		hopperMotorSpeed = SmartDashboard.getNumber("Hopper (indexer) motor power: ", hopperMotorSpeed);
		shootingMotorSpeed = SmartDashboard.getNumber("Shooter motor power: ", shootingMotorSpeed);
		intakeMotorSpeed = SmartDashboard.getNumber("Intake motor power: ", intakeMotorSpeed);
		extraTimePC = SmartDashboard.getNumber("Extra time between Power Cells: ", extraTimePC);
		desiredShooterMotorSpeed = SmartDashboard.getNumber("Desired SHooter motor speed: ", desiredShooterMotorSpeed);
		shootingMotorSpeedHighPower = SmartDashboard.getNumber("High Power Shooter motor power: ", shootingMotorSpeedHighPower);
		desiredShooterMotorSpeed = SmartDashboard.getNumber("PID - shooter motor velocity", desiredShooterMotorSpeed);
		SmartDashboard.putNumber("tx", tx.getDouble(0));
		SmartDashboard.putNumber("ty", ty.getDouble(0));
		SmartDashboard.putNumber("tv", tv.getDouble(0));
		SmartDashboard.putNumber("ta", ta.getDouble(0));
		SmartDashboard.putNumber("Distance to target: ", distance_to_target);
}

public void manageShootingMotor() 
{

	if (!shootingMotorOn)
	{
		shooter_motor.set(0);
		rampStartTime = -1;
	}

	else
	{
		shooter_motor.set(shootingMotorSpeed);
		rampStartTime = current_time;;
	}
	
	if (rampStartTime + shootingRampTime <= current_time && rampStartTime >= 0 
	&& timeLastShot + shootingRampTime <= current_time) 
	{
		shootingMotorRamped = true;
	}

	else
	{
		shootingMotorRamped = false;
	}
}

public void shootPowerCells()
{
	if (shootingMotorRamped == true)
	{
		isCurrentlyShooting = true;
		hopper_motor.set(hopperMotorSpeed);
	}

	else
	{
		isCurrentlyShooting = false;
		if(hopperOveride == false)
		{
			hopper_motor.set(0);
		}
		manageShootingMotor();
	}
}

public void trackPowerCells()
	{
		if (powercellsIntakePIN.get() == true) {
			//Only increment if the right amount of time has elapsed and the PIN is high and hasn't already
			//been incremented for this cycle
			if(current_time > last_intake_time_powerCellMS + intake_timeoutMS && !powercellsIntakePIN_already_true)
			{
				//reset the last time there was an intake
				last_intake_time_powerCellMS = current_time;
				//increment the count if greater than 0
				if (powerCellsInRobot < 5)
				{
					hopper_motor.set(hopperMotorSpeed);
					powerCellsInRobot++;
					leds.lightLEDS(0, powerCellsInRobot-1);
				}
			}
			powercellsIntakePIN_already_true = true;
		}
		//This lets us know that the sensor has turned off (so that a power cell sitting in front of the sensor only gets counted once)
		else
		{
			powercellsIntakePIN_already_true = false;
		} 
		if(powercellsOutputPIN.get() == true) {
			//Only increment if the right amount of time has elapsed and the PIN is high and hasn't already
			//been incremented for this cycle
			if(current_time > last_output_time_powerCellMS + output_timeoutMS)
			{
				//reset the last time there was an intake
				last_output_time_powerCellMS = current_time;
				//decrement the count if more than 0 
				if (powerCellsInRobot > 0)
				{
					powerCellsInRobot--;
					timeLastShot = current_time;

					//Only update 
					leds.lightLEDS(0, powerCellsInRobot-1);
				}
			}
			powercellsOutputPIN_already_true = true;
		}
		//This lets us know that the sensor has turned off (so that a power cell sitting in front of the sensor only gets counted once)
		else
		{
			powercellsOutputPIN_already_true = false;
		}
		//Manage the hopper motor
		if (powercellsIntakePIN.get() == false && isCurrentlyShooting == false)
		{
			//hopper_motor.set(0);
		}
		System.out.printf("LEDS: %d", powerCellsInRobot);
	}

/******************************************************************************************************
*******************************************************************************************************
										AUTON FUNCTIONS
******************************************************************************************************
*******************************************************************************************************/
	/**
	 * This function is run once each time the robot enters autonomous mode
	 */
	@Override
	public void autonomousInit()
	{
		kP = SmartDashboard.getNumber("PID - P: ", kP);
		kI = SmartDashboard.getNumber("PID - I: ", kI);
		kD = SmartDashboard.getNumber("PID - D: ", kD);
		driveSpeed_forward = SmartDashboard.getNumber("Auton drive speed: ", driveSpeed_forward);
		correctionSideways = SmartDashboard.getNumber("Auton SLIDE Correction: ", correctionSideways);	
		autonStart = SmartDashboard.getString("Auton start position (left, right, center)", "left");
		powerCellsInRobot = (int) SmartDashboard.getNumber("starting number of powercells auton", 3);	
		hopperMotorSpeed = SmartDashboard.getNumber("Hopper (indexer) motor power: ", hopperMotorSpeed);
		shootingMotorSpeed = SmartDashboard.getNumber("Shooter motor power: ", shootingMotorSpeed);
		intakeMotorSpeed = SmartDashboard.getNumber("Intake motor power: ", intakeMotorSpeed);
		autonC_initBackup = SmartDashboard.getNumber("CENTER - init backup: ", autonC_initBackup);
		autonC_center2Wall = SmartDashboard.getNumber("CENTER - center to wall: ", autonC_center2Wall);
		autonC_wall2trench = SmartDashboard.getNumber("CENTER - wall to-through trenchrun: ", autonC_wall2trench);
		autonL_initBackup = SmartDashboard.getNumber("LEFT - init backup: ", autonL_initBackup);
		autonR_initBackup = SmartDashboard.getNumber("RIGHT - init backup: ", autonR_initBackup);
		autonR_wall2trench = SmartDashboard.getNumber("RIGHT - wall to-through trenchrun: ", autonR_wall2trench);
		auton_Powercellretrive = SmartDashboard.getString("Auton - only get three powercells:", auton_Powercellretrive);
		auton_drivedirection = SmartDashboard.getString("Auton - drive forward: ", auton_drivedirection);
		rightautonwallangle = SmartDashboard.getNumber("RIGHT - wall angle", rightautonwallangle);
		rightautonbackupangle = SmartDashboard.getNumber("RIGHT - back up angle", rightautonbackupangle);
		shootingMotorSpeedHighPower = SmartDashboard.getNumber("High Power Shooter motor power: ", shootingMotorSpeedHighPower);
		distance_to_target = SmartDashboard.getNumber("Distance to target: ", distance_to_target);
		auton_step = 0;
		NavX.reset();
		
		RB_2020.configFactoryDefault();
		LB_2020.configFactoryDefault();
		RF_2020.configFactoryDefault();
		LF_2020.configFactoryDefault();
		RB_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		LB_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		RF_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		LF_2020.configSelectedFeedbackSensor(FeedbackDevice.IntegratedSensor,0,10);
		RB_2020.setSelectedSensorPosition(0);
		LB_2020.setSelectedSensorPosition(0);
		LF_2020.setSelectedSensorPosition(0);
		RF_2020.setSelectedSensorPosition(0);
		RB_2020.setNeutralMode(NeutralMode.Coast);
		LB_2020.setNeutralMode(NeutralMode.Coast);
		RF_2020.setNeutralMode(NeutralMode.Coast);
		LF_2020.setNeutralMode(NeutralMode.Coast);
		elevator_motor.setIdleMode(IdleMode.kBrake);
		current_time = System.currentTimeMillis();
	}
	/**
	 * This function is called periodically during autonomous
	 */
	@Override
	public void autonomousPeriodic() 
	{if( 47<currentDistance <52) {
		//then Red 1
		} else if( 57<currentDistance <62) {
		//then Red 2
		} else if( 67<currentDistance <72) {
		//then Blue 1
		} else if( 77< currentDistance <82) {
		//then Blue 2
		}
		
		current_time = System.currentTimeMillis();
		
		print_counter++;
		/***************************LIMELIGHT CODE**********************************/
		//read values periodically
		x = tx.getDouble(0.0) + x_offset;
		y = ty.getDouble(0.0);
		area = ta.getDouble(0.0);
		v = tv.getDouble(-1);

		distance_to_target = ((29 - 5.5) / java.lang.Math.tan(5 + y));
		/***************************END LIMELIGHT CODE**********************************/
		//trackPowerCells();
		if (autonStart.equalsIgnoreCase("CENTER"))
		{
			if(auton_step == 0)
			{
				if(startAutonStepTime < 0)
				{
					startAutonStepTime = current_time;
				}
				if(current_time < startAutonStepTime + 200)
				{
					shooter_motor.set(-1);
				}
				else if(current_time < startAutonStepTime + 500)
				{
					shooter_motor.set(shootingMotorSpeed);
				}
				//turn on hooper motor to shoot
				else if (current_time < startAutonStepTime + 750)
				{
					hopper_motor.set(hopperMotorSpeed/2);
				}
				if (current_time > startAutonStepTime + 3000)
				{
					shooter_motor.set(0);
					hopper_motor.set(0);
					auton_step++;
					startAutonStepTime = -1;
				}
				//shootPowerCells();
				
			}
			else if (auton_step == 1) {
				//Timer.delay(25);
				//Back up
				
				if(auton_drivedirection.equalsIgnoreCase("back"))
				{
					auton_drive(autonC_initBackup, drive_Forward, -driveSpeed_forward, 0, true, 0);
				}
				else
				{
					auton_drive(autonC_initBackup, drive_Forward, driveSpeed_forward, 0, true, 0);
				}
				if(!auton_drivedirection.equalsIgnoreCase("back")||auton_Powercellretrive.equalsIgnoreCase("3"))
				{
					auton_step = 99;

				}
				else
				{
					auton_step++;
				}
			} 
			else if (auton_step == 2) {
				
				//turn toward trench wall
				
				if(startAutonStepTime < 0)
				{
					turnController.enable();
					turnController.setSetpoint(-90);
					startAutonStepTime = current_time;
				}
				DriveTrain.driveCartesian(0, 0, -currentRotationRate);
				if(current_time > startAutonStepTime + 1500)
				{
					auton_step ++;
					startAutonStepTime = -1;
					intakeArmSolenoid.set(Value.kReverse);
					intakemotor.set(intakeMotorSpeed);
				}	
			} 
			else if (auton_step == 3)
			{
				auton_drive(autonC_wall2trench, drive_Forward, -driveSpeed_forward, 0, true, -90);
				auton_step ++;
			}
			else if (auton_step == 4) {
				//turn toward trench wall
				if(startAutonStepTime < 0)
				{
					turnController.enable();
					turnController.setSetpoint(0);
					startAutonStepTime = current_time;
				}
				DriveTrain.driveCartesian(0, 0, -currentRotationRate);
				if(current_time > startAutonStepTime + 1500)
				{
					auton_step ++;
					startAutonStepTime = -1;
				}	
			} 
			else if (auton_step == 5) {
				//Move backward towards trench run while spinning intake, picking up powercells
				auton_drive(autonC_wall2trench, drive_Forward, -driveSpeed_forward*0.4, 0, true, 0);
				auton_step ++;
			} 
			else if (auton_step == 6) {
				//move forward to previous position
				auton_drive(autonC_wall2trench, drive_Forward, driveSpeed_forward, 0, true, 0);
				auton_step ++;
			} 
			else if (auton_step == 7) {
				//turn toward trench wall
				if(startAutonStepTime < 0)
				{
					turnController.enable();
					turnController.setSetpoint(90);
					startAutonStepTime = current_time;
				}
				DriveTrain.driveCartesian(0, 0, -currentRotationRate);
				if(current_time > startAutonStepTime + 1500)
				{
					auton_step ++;
					startAutonStepTime = -1;
				}	
			}
			else if (auton_step == 8)
			{
				auton_drive(autonC_center2Wall*0.8, drive_Forward, -driveSpeed_forward, 0, true, 90);
				auton_step ++;

			} 
			else if (auton_step == 9) {
				//turn toward trench wall
				if(startAutonStepTime < 0)
				{
					turnController.enable();
					turnController.setSetpoint(0);
					startAutonStepTime = current_time;
					shooter_motor.set(shootingMotorSpeed);
				}
				DriveTrain.driveCartesian(0, 0, -currentRotationRate);
				if(current_time > startAutonStepTime + 1500)
				{
					auton_step ++;
					startAutonStepTime = -1;
				}	
			}
			else if (auton_step == 10) {
				//move forward to step 1 position
				auton_drive(autonC_initBackup*0.5, drive_Forward, driveSpeed_forward, 0, true, 0);
				if(startAutonStepTime < 0)
				{
					RB_2020.setNeutralMode(NeutralMode.Brake);
					LB_2020.setNeutralMode(NeutralMode.Brake);
					RF_2020.setNeutralMode(NeutralMode.Brake);
					LF_2020.setNeutralMode(NeutralMode.Brake);
					startAutonStepTime = current_time;
				}
				if(current_time > startAutonStepTime + 200)
				{
					auton_step ++;
					startAutonStepTime = -1;
				}
			} 
			else if (auton_step == 11) {
				
				
				//shoot powercells
				hopper_motor.set(hopperMotorSpeed/2);
			}
		}
		else if (autonStart.equalsIgnoreCase("LEFT"))
		{
			if(auton_step == 0)
			{
				if(startAutonStepTime < 0)
				{
					startAutonStepTime = current_time;
				}
				if(current_time < startAutonStepTime + 900)
				{
					shooter_motor.set(-1);
				}
				else if(current_time < startAutonStepTime + 2500)
				{
					shooter_motor.set(shootingMotorSpeed*.99);
				}
				//turn on hooper motor to shoot
				else if (current_time < startAutonStepTime + 3100)
				{
					hopper_motor.set(hopperMotorSpeed/2);
				}
				if (current_time > startAutonStepTime + 7500)
				{
					shooter_motor.set(0);
					hopper_motor.set(0);
					auton_step++;
					startAutonStepTime = -1;
				}
				//shootPowerCells();
				
			}
			else if (auton_step == 1) {
				//Timer.delay(25);
				//Back up
				
				if(auton_drivedirection.equalsIgnoreCase("back"))
				{
					auton_drive(autonL_initBackup, drive_Forward, -driveSpeed_forward, 0, true, 0);
				}
				else
				{
					auton_drive(autonL_initBackup, drive_Forward, driveSpeed_forward, 0, true, 0);
				}
				if(!auton_drivedirection.equalsIgnoreCase("back")||auton_Powercellretrive.equalsIgnoreCase("3"))
				{
					auton_step = 99;
				}
				else
				{
					auton_step++;
				}
			} 
		}
		else if (autonStart.equalsIgnoreCase("RIGHT"))
		{	
			if(auton_step == 0)
			{
				if(startAutonStepTime < 0)
				{
					startAutonStepTime = current_time;
				}
				if(current_time < startAutonStepTime + 900)
				{
					shooter_motor.set(-1);
				}
				else if(current_time < startAutonStepTime + 1600)
				{
					shooter_motor.set(shootingMotorSpeed*.99);
				}
				//turn on hooper motor to shoot
				else if (current_time < startAutonStepTime + 2200)
				{
					hopper_motor.set(hopperMotorSpeed/2);
				}
				if (current_time > startAutonStepTime + 6750)
				{
					shooter_motor.set(0);
					hopper_motor.set(0);
					auton_step++;
					startAutonStepTime = -1;
				}
				//shootPowerCells();
				
			}
			else if (auton_step == 1) {
				intakeArmSolenoid.set(Value.kReverse);
				intakemotor.set(intakeMotorSpeed*1.2);
				//Back up
				if(auton_drivedirection.equalsIgnoreCase("back"))
				{
					auton_drive(autonR_initBackup, drive_Forward, -driveSpeed_forward, 0, true, 0);
				}
				else
				{
					auton_drive(autonR_initBackup, drive_Forward, driveSpeed_forward, 0, true, rightautonbackupangle);
				}
				if(!auton_drivedirection.equalsIgnoreCase("back")||auton_Powercellretrive.equalsIgnoreCase("3"))
				{
					auton_step = 99;
				}
				else
				{
					auton_step++;
				}
			} 
			else if (auton_step == 2){
				//Backup through the trench run
				shooter_motor.set(shootingMotorSpeed);
				auton_drive(autonR_wall2trench, drive_Forward, -driveSpeed_forward*0.5, 0, true, rightautonwallangle);//?32
				auton_step ++;
			}
			else if (auton_step == 3){
				//drive back to start of trench run
				auton_drive(autonR_wall2trench, drive_Forward, driveSpeed_forward, 0, true, rightautonwallangle);				
				auton_step ++;
			}
			else if (auton_step == 4){
				//Back up to init line
				auton_drive(autonR_initBackup, drive_Forward, driveSpeed_forward, 0, true, rightautonbackupangle);
				auton_step ++;
			}
			else if (auton_step == 5){
				//stop the robot
				RB_2020.setNeutralMode(NeutralMode.Brake);
				LB_2020.setNeutralMode(NeutralMode.Brake);
				RF_2020.setNeutralMode(NeutralMode.Brake);
				LF_2020.setNeutralMode(NeutralMode.Brake);
				auton_step ++;
			}
			else if (auton_step == 6)
			{
				if(startAutonStepTime < 0)
				{
					startAutonStepTime = current_time;
				}//turn on hooper motor to shoot
				if (current_time > startAutonStepTime + 500)
				{
					hopper_motor.set(hopperMotorSpeed/2);
					startAutonStepTime = -1;
				}
				//Shoot the power cells
			}
		}
		else 
		{
			if(auton_step == 0)
			{
				if(startAutonStepTime < 0)
				{
					startAutonStepTime = current_time;
				}
				if(current_time < startAutonStepTime + 900)
				{
					shooter_motor.set(-1);
				}
				else if(current_time < startAutonStepTime + 2500)
				{
					shooter_motor.set(shootingMotorSpeed*.99);
				}
				//turn on hooper motor to shoot
				else if (current_time < startAutonStepTime + 3100)
				{
					hopper_motor.set(hopperMotorSpeed/3);
				}
				if (current_time > startAutonStepTime + 7500)
				{
					shooter_motor.set(0);
					hopper_motor.set(0);
					auton_step++;
					startAutonStepTime = -1;
				}
				//shootPowerCells();
				
			}
			else if (auton_step == 1) {
				//Timer.delay(25);
				//Back up
				auton_drive(95.125, drive_Forward, -driveSpeed_forward, 0, true, 0);
				auton_step ++;
			}
		}
	if(auton_step == 99)
	{
		shooter_motor.set(0);
		intakemotor.set(0);
		hopper_motor.set(0);
	}
	// //	Timer.delay(1);
	// 	RB_2020.setNeutralMode(NeutralMode.Coast);
	// 	LB_2020.setNeutralMode(NeutralMode.Coast);
	// 	RF_2020.setNeutralMode(NeutralMode.Coast);
	// 	LF_2020.setNeutralMode(NeutralMode.Coast);
	distance_to_target = SmartDashboard.getNumber("Distance to target: ", distance_to_target);	
	}	
	/**
	 * Function that will drive at a desired angle (using a gyro for correction) a specified distance at a specified power
	 * Function will also move a elevator motor while driving forward a specified distance according to elevator encoder value
	 */
	public void auton_drive(double desired_distance, Boolean driveForward, double robot_drive_power, double elevator_hold_position, boolean ignore_ramp_up, double desired_angle )  {
		RB_2020.setNeutralMode(NeutralMode.Coast);
		LB_2020.setNeutralMode(NeutralMode.Coast);
		RF_2020.setNeutralMode(NeutralMode.Coast);
		LF_2020.setNeutralMode(NeutralMode.Coast);
		//RESET ENCODERS TO 0
		//Timer.delay(.1);
		double RB_start_pos = RB_2020.getSelectedSensorPosition();
		double RF_start_pos = RF_2020.getSelectedSensorPosition();
		double LB_start_pos = LB_2020.getSelectedSensorPosition();
		double LF_start_pos = LF_2020.getSelectedSensorPosition();
		double RB_current_pos = RB_start_pos;
		double RF_current_pos = RF_start_pos;
		double LB_current_pos = LB_start_pos;
		double LF_current_pos = LF_start_pos;
		

		// desired_distance = robot_drive_power* 48.667 + 19.567;
		desired_distance = desired_distance * 1025 * 1.09  * driveEncoderCorrectionFactor;

		left_wheel_distance_traveled = 0;
		right_wheel_distance_traveled = 0;
		//Cuts down on the prints
		int print_counter = 0;
		
		double watch_dog_timer = 750 + Math.abs(50*(desired_distance/robot_drive_power));
		
		//Set the PID
		turnController.setSetpoint(desired_angle);
		turnController.enable();
		//reset the encoder before starting
		
		//Get the distances each wheel has traveled
		
        //Get the angle
    	angle = NavX.getAngle();
    	
		//Get the start time for the power ramp up
		long start_time = current_time;
		//stores the elapsed time (System.currentTimeMillis() - start_time)
		long elapsedtime = 0;
		//Calculated ramp power. Will go to full robot_drive_power after ramp time completes
		double ramppower = 0;

		while (isEnabled() && Math.abs(left_wheel_distance_traveled) < Math.abs(desired_distance) &&
		Math.abs(right_wheel_distance_traveled) < Math.abs(desired_distance) && 
		watch_dog_timer > (System.currentTimeMillis() - start_time)) 
		{
			//Get the reading
			RB_current_pos = RB_2020.getSelectedSensorPosition();
			RF_current_pos = RF_2020.getSelectedSensorPosition();
			LB_current_pos = LB_2020.getSelectedSensorPosition();
			LF_current_pos = LF_2020.getSelectedSensorPosition();

			//Get the angle
        	angle = NavX.getAngle(); 
        	
        	//Get the distances each wheel has traveled
			if (Math.abs(LF_current_pos - LF_start_pos) > Math.abs(LB_current_pos - LB_start_pos))
			{
				left_wheel_distance_traveled = LF_current_pos - LF_start_pos;
			}
			else 
			{
				left_wheel_distance_traveled = LB_current_pos - LB_start_pos;	
			}
			
			if (Math.abs(RF_current_pos - RF_start_pos) > Math.abs(RB_current_pos - RB_start_pos))
			{
				right_wheel_distance_traveled = RF_current_pos - RF_start_pos;
			}
			else 
			{
				right_wheel_distance_traveled = RB_current_pos - RB_start_pos;	
			}

    		elapsedtime = System.currentTimeMillis()-start_time; 
    		if(elapsedtime < 250 && !ignore_ramp_up) 
    		{
    			ramppower = elapsedtime*0.004*robot_drive_power; 
    		}
    		
    		else if(elapsedtime >= 250 || ignore_ramp_up)
    		{
    			ramppower = robot_drive_power; 
    		}
            //Set the drive speed (auton_drive_power) and the angle (desired_angle + (current angle of the robot * the drive power * Kp [correction factor]))
			if (driveForward) 
			{
				DriveTrain.driveCartesian(ramppower, 0, -currentRotationRate);//, NavX.getAngle());
			} else {
				DriveTrain.driveCartesian(correctionSideways , ramppower, -currentRotationRate);//, NavX.getAngle());
			}
    		//print the angle every 50th iteration
            if(print_counter % 50 == 0)
            {
            	System.out.printf("Angle while running: %f\n", angle);
            	System.out.printf("Right Encoder while running: %f\n", right_wheel_distance_traveled);
            	System.out.printf("Left Encoder while running: %f\n", left_wheel_distance_traveled);
			}
			
            if (powercellsIntakePIN.get() == false)
			{
				hopper_motor.set(hopperMotorSpeed*2);
				hopperPCdelay = true;
			}
			else
			{
				if (hopperPCdelay == true)
				{
					hopperPCtime = System.currentTimeMillis();
					hopperPCdelay = false;
				}

				if (System.currentTimeMillis() > hopperPCtime + extraTimePC*2)
				{
					hopper_motor.set(0);
				}
			}	
            //Delay to slow down so that it does not suck up 100% of CPU time
           // Timer.delay(0.004);
            //increment the print counter
			print_counter++;
        }//end while loop
		RB_2020.setNeutralMode(NeutralMode.Brake);
		LB_2020.setNeutralMode(NeutralMode.Brake);
		RF_2020.setNeutralMode(NeutralMode.Brake);
		LF_2020.setNeutralMode(NeutralMode.Brake);
		//Stop the secondary motor at the end no matter what (to be safe) 
		//elevator_motor.set(0);
	}
		
	/**
	 * OVERLOAD
	 * This version holds the motor in position and assumes to ignore the ramp up
	 */
	public void auton_drive(double desired_distance, double robot_drive_power, double desired_angle) {
		auton_drive(desired_distance, false, robot_drive_power, 0, true, desired_angle);
	}
	/**
	 * OVERLOAD
	 * This version assumes to ignore the ramp up
	 */
	public void auton_drive(double desired_distance, double robot_drive_power, double hold_position, double desired_angle ) {
		auton_drive( desired_distance, false, robot_drive_power, hold_position, true, desired_angle);
	}

/******************************************************************************************************
*******************************************************************************************************
										MISC CODE
******************************************************************************************************
*******************************************************************************************************/


	public void testPeriodic() {
//		LiveWindow.run();
	}
	
	  @Override
	  /* This function is invoked periodically by the PID Controller, */
	  /* based upon navX-MXP yaw angle input and PID Coefficients.    */
	  public void pidWrite(double output) {
		currentRotationRate = output;
	}

//***************************************************************************************************
//										LIDAR CODE
//***************************************************************************************************/

public double distanceFromTargetLIDAR() 
{
	distanceFromTargetLIDAR = LIDARinput.getDistance();

	return distanceFromTargetLIDAR;
}
	

/******************************************************************************************************
*******************************************************************************************************
										COMMENTED OUT CODE
******************************************************************************************************
*******************************************************************************************************/





	
	// 	/***************************LIMELIGHT CODE**********************************/
	// 	//read values periodically
	// 	x = tx.getDouble(0.0) + x_offset;
	// 	y = ty.getDouble(0.0);
	// 	area = ta.getDouble(0.0);
	// 	v = tv.getDouble(-1);

	// 	distance_to_target = ((29 - 5.5) / java.lang.Math.tan(5 + y));
	// 	/***************************END LIMELIGHT CODE**********************************/
		
	// 	/***************************LED CONTROLS BASED ON LIMELIGHT*********************/
	// // 	if (x == 0 && y == 0 && v == 0)
	// // 	{
	// // 		LED_color_controller.setPWMRate(WHITE);
	// // 		current_color = WHITE;
	// // 	}	
	// // 	else if (x > 0 && x <= 360 && v == 1)
	// // 	{
	// // 		LED_color_controller.setPWMRate(GREEN_LEFT);
	// // 		current_color = GREEN_LEFT;
	// // 	}	
	// // 	else if (x < 0 && v == 1)
	// // 	{
	// // 		LED_color_controller.setPWMRate(GREEN_RIGHT);
	// // 		current_color = GREEN_RIGHT;
	// // 	}	
	// // 	else if (x > 360 && y > 360 && area > 100 && v == -1)
	// // 	{
	// // 		LED_color_controller.setPWMRate(RED);
	// // 		current_color = RED;
	// // 	}
		
	// // 	/**************************END LED CONTROLS BASED ON LIMELIGHT*******************/


	// 	/**************************************LOGGING INFO********************************************/
	// 	robotvoltage = pdp.getVoltage();
	// 	rightfrontmotor_amps = pdp.getCurrent(2); 
	// 	leftfrontmotor_amps= pdp.getCurrent(12); 
	// 	rightrearmotor_amps= pdp.getCurrent(3); 
	// 	leftrearmotor_amps= pdp.getCurrent(13); 
	// 	elevatormotor_amps= pdp.getCurrent(14); 
	// 	rightintakemotor_amps= pdp.getCurrent(0); 
	// 	leftintakemotor_amps = pdp.getCurrent(1); 
			
	// 	//print the angle every tenth iteration
    //     if(print_counter % 50 == 0)
    //     {
    //     	System.out.printf("Elevator in TeleOp: %f\n", elevator_distance_traveled);
    //     	System.out.printf("Right Encoder in TeleOp: %f\n", right_wheel_distance_traveled);
    //     	System.out.printf("Left Encoder in TeleOp: %f\n", left_wheel_distance_traveled);
    //     	System.out.printf("Gyro in teleOp: %f\n", angle);
    //     	System.out.printf("Right front motor_amps in teleOp: %f\n", rightfrontmotor_amps);
    //     	System.out.printf("Left front motor_amps: %f\n", leftfrontmotor_amps);
    //     	System.out.printf("Right rear motor_amps in teleOp: %f\n", rightrearmotor_amps);
    //     	System.out.printf("Left rear motor_amps in teleOp: %f\n", leftrearmotor_amps);
    //     	System.out.printf("Elevator motor_amps in teleOp: %f\n", elevatormotor_amps);
    //     	System.out.printf("Right intake motor_amps in teleOp: %f\n", rightintakemotor_amps);
    //     	System.out.printf("Lef intake motor_amps in teleOp: %f\n", leftintakemotor_amps);
	// 		System.out.printf("Rebot voltage in teleOp: %f\n", robotvoltage);
	// 		System.out.printf("left joystick POV: %d\n", left_joy_stick.getPOV());
    //     }
    //     //increment
	// 	print_counter++;

	// 	//post to smart dashboard periodically
	// 	//Limelight
	// 	SmartDashboard.putNumber("Lightlight X:", x);
	// 	SmartDashboard.putNumber("Lightlight Y:", y);
	// 	SmartDashboard.putNumber("Lightlight Area:", area);
	// 	SmartDashboard.putNumber("Distance To Target:", distance_to_target);
	// 	x_offset = SmartDashboard.getNumber("Limelight X Offset:", x_offset);
		
	// 	//Encoders
	// 	SmartDashboard.putNumber("left encoder", left_wheel_distance_traveled);
	// 	SmartDashboard.putNumber("right encoder", right_wheel_distance_traveled);
	// 	SmartDashboard.putNumber("elevator encoder", elevator_distance_traveled);
	// 	SmartDashboard.putNumber("cargo angle encoder", 0/*cargo_angle_motor.getSensorCollection().getPulseWidthPosition()*/);
	// 	//PID Values
	// 	kP = SmartDashboard.getNumber("PID P", kP);
	// 	kI = SmartDashboard.getNumber("PID I", kI);
	// 	kD = SmartDashboard.getNumber("PID D", kD);
	// 	turnController.setP(kP);
	// 	turnController.setI(kI);
	// 	turnController.setD(kD);
	// 	//Gyro
	// 	SmartDashboard.putNumber("Angle", angle);

	// 	//Joystick values
	// 	SmartDashboard.putNumber("Right Joystick", -rightjoystick_updown);
	// 	SmartDashboard.putNumber("Left Joystick", -leftjoystick_updown);
	// 	rightjscorrection = SmartDashboard.getNumber("Right Correction", 0);
	// 	leftjscorrection = SmartDashboard.getNumber("Left Correction", 0);
		  
	// 	/**************************************END LOGGING INFO********************************************/

}

